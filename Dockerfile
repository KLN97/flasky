FROM python:3.8-alpine

RUN mkdir /code

COPY requirements.txt /code/

RUN pip install -r /code/requirements.txt

COPY app.py /code/

COPY kubrick.db /code/

COPY index.html /code/

CMD ["python","/code/app.py"]