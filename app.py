from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String
import os
from flask_marshmallow import Marshmallow
app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
basedir=r'C:\Users\KhilanShah\projects\MLE02flask'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'kubrick.db')
db = SQLAlchemy(app)
ma = Marshmallow(app)

#home endpoint
@app.route('/')
def home():
    return jsonify(data='My MLE02 Home Page - Welcome!!!!')

# create another endpoint listener /test
# get it to return a messgae acknowledging you have arrived at the TEST endpoint
# test the new endpoint with http://127.0.0.1:5000/test
#home endpoint
# add an parameter option for the /test endpoint, where the user supplies a number
# the api endpoint mulitples by 2... whatevs. and returns
# http://127.0.0.1:5000/test?number=23
@app.route('/test')
def test():
    num = int(request.args.get('number', 0))
    return jsonify(data='The answer is {}'.format(num*2))


class Consultant(db.Model):
    __tablename__ = 'consultant'
    id = Column(Integer, primary_key=True)
    fname = Column(String)
    lname = Column(String)
    cohort = Column(String)
    email = Column(String, unique=True)
class ConSchema(ma.Schema):
    class Meta:
        fields = ('id', 'fname', 'lname', 'cohort', 'email')
con_schema = ConSchema()
con_schemas=ConSchema(many=True)

# create a new endpoint called consultant
# http://localhost:5000/consultant?email=lawrencefreeman@kubrickgroup.com
# the endpoint must grab the email variable from the querystring
# ... and assign it to a variable called con
@app.route('/consultant')
def consultant():
    con = request.args.get('email', False)
    cohort_query=request.args.get('cohort', False)
    if con:
        data = Consultant.query.filter_by(email=con).first()
        result1 = con_schema.dump(data)
        return jsonify(result1)
    if cohort_query:
        cohort_data=Consultant.query.filter_by(cohort=cohort_query).all()
        result2 = con_schemas.dump(cohort_data)
        return jsonify(result2)




if __name__ == '__main__':
    app.run()